
-- SUMMARY --

The Relation UUID module enables you to featurize relations through the use of
UUID's. Currently this module only provides support for relations with taxonomy
endpoints. Support for other endpoint entities is work in progress.

For a full description of the module, visit the project page:
  https://drupal.org/sandbox/Mywebmaster/2075541

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2075541?status=All&categories=All


-- REQUIREMENTS --

* Relation
* Features
* UUID


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONTACT --

Current maintainers:
* Alex Verbruggen (alexverb) - https://drupal.org/user/1129948
