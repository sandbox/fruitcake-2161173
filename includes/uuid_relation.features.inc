<?php
/**
 * @file
 * Features hooks for the uuid_relation features component.
 */

/**
 * Implements hook_features_export_options().
 *
 * TODO:  Implement caching mechanism? Is that smart for this use case?
 */
function uuid_relation_features_export_options($component) {
  $relation_type = str_replace('uuid_relation_', '', $component);
  $options = array();
  // Query relations for components relation type.
  $query = db_query('SELECT r.rid FROM {relation} r WHERE r.relation_type = :relation_type', array(':relation_type' => $relation_type));
  $rids = $query->fetchCol();
  $relations = relation_load_multiple($rids);
  foreach ($relations as $relation) {
    $endpoints = array();
    // Get the endpoints to represent relation later on.
    foreach ($relation->endpoints[LANGUAGE_NONE] as $endpoint) {
      $entity_type = $endpoint['entity_type'];
      $entity_info = entity_get_info($entity_type);
      $entity = entity_load($entity_type, array($endpoint['entity_id']));
      $entity = reset($entity);
      $endpoints[] = check_plain($entity->{$entity_info['entity keys']['label']});
    }
    // Build the option strings.
    $options[$relation->uuid] = t('!endpoints', array(
      '!endpoints' => implode(' → ', $endpoints),
    ));
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function uuid_relation_features_export($data, &$export, $module_name = '') {
  $pipe = array();

  $rids = entity_get_id_by_uuid('relation', $data);
  foreach ($rids as $uuid => $rid) {
    // Load the existing relation, with a fresh cache.
    $relation = relation_load($rid, NULL, TRUE);
    if ($relation) {
      foreach ($relation->endpoints[LANGUAGE_NONE] as $endpoint) {
        $entity_type = $endpoint['entity_type'];
        $entity_id = $endpoint['entity_id'];
        $entity = entity_load($entity_type, array($entity_id));
        $entity = reset($entity);
        $entity_uuid = $entity->uuid;
        switch ($entity_type) {

          case 'taxonomy_term':
            // Add entities.
            $export['features']['uuid_term'][$entity_uuid] = $entity_uuid;
            $export['features']['taxonomy'][$entity->vocabulary_machine_name] = $entity->vocabulary_machine_name;
            break;

          case 'taxonomy':

            // Add dependency.
            $export['dependencies']['taxonomy'] = 'taxonomy';
            // Add entity.
            $export['features']['taxonomy'][$entity->vocabulary_machine_name] = $entity->vocabulary_machine_name;
            break;

          default:

            // TODO: Support all other entity types.
        }
      }
      $export['features']['uuid_relation_' . $relation->relation_type][$uuid] = $uuid;
      $pipe['relation_type'][$relation->relation_type] = $relation->relation_type;
    }

    // drupal_alter() normally supports just one byref parameter. Using
    // the __drupal_alter_by_ref key, we can store any additional parameters
    // that need to be altered, and they'll be split out into additional params
    // for the hook_*_alter() implementations.  The hook_alter signature is
    // hook_uuid_relation_features_export_alter(&$export, &$pipe, $relation).
    $data = &$export;
    $data['__drupal_alter_by_ref'] = array(&$pipe);
    drupal_alter('uuid_relation_features_export', $data, $relation);
  }

  return $pipe;
}

/**
 * Swap endpoint entity ids for uuids.
 *
 * @param object $relation
 *   Full relation object containing the endpoints
 */
function uuid_relation_set_endpoints_uuid($relation) {
  foreach ($relation->endpoints[LANGUAGE_NONE] as &$endpoint) {
    $entity_id = array_values(entity_get_uuid_by_id($endpoint['entity_type'], array($endpoint['entity_id'])));
    $entity_id = reset($entity_id);
    $endpoint['entity_id'] = $entity_id;
  }
  return $relation;
}


/**
 * Implements hook_features_export_render().
 */
function uuid_relation_features_export_render($module, $data) {
  $translatables = $code = array();

  uuid_features_load_module_includes();

  $code[] = '  $relations = array();';
  $code[] = '';
  $rids = entity_get_id_by_uuid('relation', $data);
  foreach ($rids as $rid) {
    // Only export the relation if it exists.
    if ($rid === FALSE) {
      continue;
    }
    // Attempt to load the relation, using a fresh cache.
    $relation = relation_load($rid, NULL, TRUE);
    if (empty($relation)) {
      continue;
    }
    $relation = uuid_relation_set_endpoints_uuid($relation);
    $export = $relation;

    // Use date instead of created, in the same format used by
    // relation_object_prepare.
    $export->date = format_date($export->created, 'custom', 'Y-m-d H:i:s O');

    // Don't cause conflicts with rid/vid/revision_timestamp/changed fields.
    unset($export->rid);
    unset($export->vid);
    unset($export->changed);

    drupal_alter('uuid_relation_features_export_render', $export, $relation, $module);

    $code[] = '  $relations[] = ' . features_var_export($export) . ';';
  }

  if (!empty($translatables)) {
    $code[] = features_translatables_export($translatables, '  ');
  }

  $code[] = '  return $relations;';
  $code = implode("\n", $code);
  if (isset($relation) && !empty($relation)) {
    return array('uuid_relation_' . $relation->relation_type => $code);
  }
}

/**
 * Implements hook_features_revert().
 */
function uuid_relation_features_revert($module, $component) {
  uuid_relation_features_rebuild($module, $component);
}

/**
 * Implements hook_features_rebuild().
 *
 * Rebuilds relations based on UUID from code defaults.
 */
function uuid_relation_features_rebuild($module, $component) {
  // Get relations for the right component.
  $relations = module_invoke($module, $component);
  if (!empty($relations)) {
    // Get the entity ids. If none is found we rebuild the endpoints first.
    foreach ($relations as $data) {
      $relation = (object) $data;
      foreach ($relation->endpoints[LANGUAGE_NONE] as $delta => $endpoint) {
        $entity_id = entity_get_id_by_uuid($endpoint['entity_type'], array($endpoint['entity_id']));
        if (empty($entity_id)) {
          uuid_term_features_rebuild($module);
          $entity_id = entity_get_id_by_uuid($endpoint['entity_type'], array($endpoint['entity_id']));
        }
        $relation->endpoints[LANGUAGE_NONE][$delta]['entity_id'] = reset($entity_id);
      }

      // Find the matching UUID, with a fresh cache.
      $rids = entity_get_id_by_uuid('relation', array($relation->uuid));
      if (isset($rids[$relation->uuid])) {
        $rid = $rids[$relation->uuid];
        $existing = relation_load($rid, NULL, TRUE);
        if (!empty($existing)) {
          $relation->rid = $existing->rid;
          $relation->vid = $existing->vid;
        }
      }

      drupal_alter('uuid_relation_features_rebuild', $relation, $module);

      uuid_relation_relation_save($relation);
    }
  }
}

/**
 * Helper function to save a relation upon rebuild.
 */
function uuid_relation_relation_save($relation) {
  // Store changed and created date so we can update db records with it later.
  $changed = $relation->revision_changed;
  $created = $relation->created;
  // Save the entity uuid.
  entity_uuid_save('relation', $relation);
  // Change created and changed date back to original.
  $relation->changed = $changed;
  $relation->created = $created;
  // Get the id and overwrite database records again.
  $entity_id = entity_get_id_by_uuid('relation', array($relation->uuid));
  $entity_id = reset($entity_id);
  $saved_relation = relation_load($entity_id);
  $relation->rid = $saved_relation->rid;
  $relation->vid = $saved_relation->vid;
  drupal_write_record('relation', $relation, array('rid'));
  drupal_write_record('relation_revision', $relation, array('vid'));
  // Also update the endpoints.
  call_user_func("field_attach_update", 'relation', $relation);
}
