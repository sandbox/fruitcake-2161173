<?php
/**
 * @file
 * uuid_relation_demo.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uuid_relation_demo_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'driv',
    'description' => 'description of driv',
    'format' => 'plain_text',
    'weight' => 6,
    'uuid' => '03f45569-7a36-416b-b29a-734fad72c11a',
    'vocabulary_machine_name' => 'gukefrunush',
  );
  $terms[] = array(
    'name' => 'wistu',
    'description' => 'description of wistu',
    'format' => 'plain_text',
    'weight' => 3,
    'uuid' => '48e1162e-b116-4e51-877e-4998e4feb168',
    'vocabulary_machine_name' => 'gukefrunush',
  );
  $terms[] = array(
    'name' => 'cavashe',
    'description' => 'description of cavashe',
    'format' => 'plain_text',
    'weight' => 0,
    'uuid' => '7390399a-64e9-4e70-8db8-f01d320fdbed',
    'vocabulary_machine_name' => 'gukefrunush',
  );
  $terms[] = array(
    'name' => 'bige',
    'description' => 'description of bige',
    'format' => 'plain_text',
    'weight' => 1,
    'uuid' => 'aa9d4993-1bbd-4070-a403-d25380a06c9b',
    'vocabulary_machine_name' => 'gukefrunush',
  );
  $terms[] = array(
    'name' => 'thudej',
    'description' => 'description of thudej',
    'format' => 'plain_text',
    'weight' => 2,
    'uuid' => 'c397c174-1975-447c-94f6-a9390622eaf8',
    'vocabulary_machine_name' => 'gukefrunush',
  );
  $terms[] = array(
    'name' => 'gebrulokapr',
    'description' => 'description of gebrulokapr',
    'format' => 'plain_text',
    'weight' => 5,
    'uuid' => 'f75a4ee2-61d3-48e2-b65d-81d3f0081f5b',
    'vocabulary_machine_name' => 'gukefrunush',
  );
  $terms[] = array(
    'name' => 'wrephelup',
    'description' => 'description of wrephelup',
    'format' => 'plain_text',
    'weight' => 4,
    'uuid' => 'f9439e06-2769-4754-98e0-8415bde7676f',
    'vocabulary_machine_name' => 'gukefrunush',
  );
  return $terms;
}
