<?php
/**
 * @file
 * uuid_relation_demo.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uuid_relation_demo_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "relation" && $api == "relation_type_default") {
    return array("version" => "1");
  }
}
