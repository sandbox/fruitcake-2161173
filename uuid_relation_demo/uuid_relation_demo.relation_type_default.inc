<?php
/**
 * @file
 * uuid_relation_demo.relation_type_default.inc
 */

/**
 * Implements hook_relation_default_relation_types().
 */
function uuid_relation_demo_relation_default_relation_types() {
  $export = array();

  $relation_type = new stdClass();
  $relation_type->disabled = FALSE; /* Edit this to true to make a default relation_type disabled initially */
  $relation_type->api_version = 1;
  $relation_type->relation_type = 'term_relations';
  $relation_type->label = 'Term relations';
  $relation_type->reverse_label = 'Term relations';
  $relation_type->directional = 1;
  $relation_type->transitive = 1;
  $relation_type->r_unique = 1;
  $relation_type->min_arity = 3;
  $relation_type->max_arity = 3;
  $relation_type->source_bundles = array(
    0 => 'taxonomy_term:gukefrunush',
  );
  $relation_type->target_bundles = array(
    0 => 'taxonomy_term:gukefrunush',
  );
  $export['term_relations'] = $relation_type;

  return $export;
}
