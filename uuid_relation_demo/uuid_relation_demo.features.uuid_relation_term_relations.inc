<?php
/**
 * @file
 * uuid_relation_demo.features.uuid_relation_term_relations.inc
 */

/**
 * Implements hook_uuid_relation_term_relations().
 */
function uuid_relation_demo_uuid_relation_term_relations() {
  $relations = array();

  $relations[] = array(
  'relation_type' => 'term_relations',
  'uid' => 1,
  'arity' => 3,
  'vuuid' => '4575dfd7-629f-42b2-a562-9eff51ae2e0c',
  'created' => 1385910062,
  'uuid' => '1cc3288a-d965-40e2-9917-3d87643339d7',
  'revision_uid' => 1,
  'revision_changed' => 1385910062,
  'endpoints' => array(
    'und' => array(
      0 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'aa9d4993-1bbd-4070-a403-d25380a06c9b',
        'r_index' => 0,
      ),
      1 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'f9439e06-2769-4754-98e0-8415bde7676f',
        'r_index' => 1,
      ),
      2 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'f75a4ee2-61d3-48e2-b65d-81d3f0081f5b',
        'r_index' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(),
  'date' => '2013-12-01 16:01:02 +0100',
);
  $relations[] = array(
  'relation_type' => 'term_relations',
  'uid' => 1,
  'arity' => 3,
  'vuuid' => '53b93e01-0dfe-4f75-832c-51239802f22f',
  'created' => 1385910015,
  'uuid' => '5294bdce-ed79-49be-9f3f-b1b6ce358fc0',
  'revision_uid' => 1,
  'revision_changed' => 1385910015,
  'endpoints' => array(
    'und' => array(
      0 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => '7390399a-64e9-4e70-8db8-f01d320fdbed',
        'r_index' => 0,
      ),
      1 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'aa9d4993-1bbd-4070-a403-d25380a06c9b',
        'r_index' => 1,
      ),
      2 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'c397c174-1975-447c-94f6-a9390622eaf8',
        'r_index' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(),
  'date' => '2013-12-01 16:00:15 +0100',
);
  $relations[] = array(
  'relation_type' => 'term_relations',
  'uid' => 1,
  'arity' => 3,
  'vuuid' => '17ceff3c-63fc-4f09-9110-85d686c543a6',
  'created' => 1385910129,
  'uuid' => 'b27d5421-7f43-43eb-8783-c1279a5c180a',
  'revision_uid' => 1,
  'revision_changed' => 1385910129,
  'endpoints' => array(
    'und' => array(
      0 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'f75a4ee2-61d3-48e2-b65d-81d3f0081f5b',
        'r_index' => 0,
      ),
      1 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => '48e1162e-b116-4e51-877e-4998e4feb168',
        'r_index' => 1,
      ),
      2 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'f9439e06-2769-4754-98e0-8415bde7676f',
        'r_index' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(),
  'date' => '2013-12-01 16:02:09 +0100',
);
  $relations[] = array(
  'relation_type' => 'term_relations',
  'uid' => 1,
  'arity' => 3,
  'vuuid' => '564ddfaf-1636-4007-b595-fc336eda4439',
  'created' => 1385910024,
  'uuid' => 'dd2d50c8-cc5c-4374-b550-4d688d8902df',
  'revision_uid' => 1,
  'revision_changed' => 1385910024,
  'endpoints' => array(
    'und' => array(
      0 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => '7390399a-64e9-4e70-8db8-f01d320fdbed',
        'r_index' => 0,
      ),
      1 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'f9439e06-2769-4754-98e0-8415bde7676f',
        'r_index' => 1,
      ),
      2 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'f75a4ee2-61d3-48e2-b65d-81d3f0081f5b',
        'r_index' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(),
  'date' => '2013-12-01 16:00:24 +0100',
);
  $relations[] = array(
  'relation_type' => 'term_relations',
  'uid' => 1,
  'arity' => 3,
  'vuuid' => 'acbe3722-578b-4707-87f6-489e173d8a44',
  'created' => 1385910103,
  'uuid' => 'ffbb33e3-8a56-4998-882e-b3e6f8917aac',
  'revision_uid' => 1,
  'revision_changed' => 1385910103,
  'endpoints' => array(
    'und' => array(
      0 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => 'f75a4ee2-61d3-48e2-b65d-81d3f0081f5b',
        'r_index' => 0,
      ),
      1 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => '03f45569-7a36-416b-b29a-734fad72c11a',
        'r_index' => 1,
      ),
      2 => array(
        'entity_type' => 'taxonomy_term',
        'entity_id' => '7390399a-64e9-4e70-8db8-f01d320fdbed',
        'r_index' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(),
  'date' => '2013-12-01 16:01:43 +0100',
);
  return $relations;
}
